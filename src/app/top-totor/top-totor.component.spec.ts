import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopTotorComponent } from './top-totor.component';

describe('TopTotorComponent', () => {
  let component: TopTotorComponent;
  let fixture: ComponentFixture<TopTotorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopTotorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopTotorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
