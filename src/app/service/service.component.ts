import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.scss']
})
export class ServiceComponent implements OnInit {

  constructor(private title: Title,
    private meta: Meta) { }

  ngOnInit(): void {
    window.scrollTo(0, 0)
    this.title.setTitle('Service');
   setTimeout(() => {  
      this.meta.updateTag(  
        { name: 'description', content: '' },  
        'name=description'  
      )  
      const description = this.meta.getTag('name=description');
if (description) console.log(description.content);
    }, 4000) 
  }

}
